//
//  GameCharactor.h
//  ro3
//
//  Created by JJ WEI on 12-06-27.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "GameObject.h"
#import "TileHelper.h"
#import "GameItem.h"

@interface GameCharacter : GameObject <CCTouchOneByOneDelegate>
{
    GameItem *accessary;
    
    CGPoint startPoint;
    CGPoint endPoint;
    CGPoint oldPosition;
    
     CGPoint originPos;
    UITouch *lastTouch;
    
    BOOL characterIsPicked;
    CGPoint velocity;
    float degrees;
    float dSqrt;
    
    CharacterStates characterState;
    
    ccTime delta;
    BOOL carryPoints;
    BOOL isDisabled;
    
    // new variable for untouchable feature
    BOOL notTouchable;
}


@property (assign) CharacterStates characterState;

-(void)checkAndClampSpritePosition;

@end
