//
//  UserPreferences.h
//  hako
//
//  Created by Jean-Jean Wei on 13-01-19.
//
//

#import <Foundation/Foundation.h>

@interface UserPreferences : NSObject <NSCoding>
{
    NSMutableArray *avator1;
    NSMutableArray *avator2;
    NSMutableArray *avator3;
    int currentAvator;     //user
    BOOL isMusicOn;  //music
    BOOL isSoundEffectOn;   // soundEffect
    int scoreAvator1;
    int scoreAvator2;
    int scoreAvator3;
}

@property (strong) NSMutableArray *avator1;
@property (strong) NSMutableArray *avator2;
@property (strong) NSMutableArray *avator3;
@property (assign) int currentAvator;
@property (assign) BOOL isMusicOn;
@property (assign) BOOL isSoundEffectOn;
@property (assign) int scoreAvator1;
@property (assign) int scoreAvator2;
@property (assign) int scoreAvator3;

@end
