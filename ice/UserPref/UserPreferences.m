//
//  UserPreferences.m
//  hako
//
//  Created by Jean-Jean Wei on 13-01-19.
//
//

#import "UserPreferences.h"

#define kLevelCount 21

@implementation UserPreferences

@synthesize avator1, avator2, avator3;
@synthesize currentAvator,isMusicOn,isSoundEffectOn;
@synthesize scoreAvator1,scoreAvator2,scoreAvator3;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        avator1 = [NSMutableArray new];
        avator2 = [NSMutableArray new];
        avator3 = [NSMutableArray new];
        currentAvator = 1;
        isMusicOn = YES;
        isSoundEffectOn = YES;
        scoreAvator1 = 0;
        scoreAvator2 = 0;
        scoreAvator3 = 0;
        
        [self creatScoreArray];
    }
    return self;
}

- (void)creatScoreArray
{
    for (int i = 0; i<kLevelCount; i++) {
        [avator1 addObject:[NSNumber numberWithInt:0]];
        [avator2 addObject:[NSNumber numberWithInt:0]];
        [avator3 addObject:[NSNumber numberWithInt:0]];
    }
}

- (id) initWithCoder: (NSCoder *)coder
{
    if (self = [super init])
    {
        avator1 = [coder decodeObjectForKey:@"avator1"];
        avator2 = [coder decodeObjectForKey:@"avator2"];
        avator3 = [coder decodeObjectForKey:@"avator3"];
        currentAvator = [coder decodeIntForKey:@"currentAvator"];
        isMusicOn = [coder decodeBoolForKey:@"isMusicOn"];
        isSoundEffectOn = [coder decodeBoolForKey:@"isSoundEffectOn"];
        scoreAvator1 = [coder decodeIntForKey:@"scoreAvator1"];
        scoreAvator2 = [coder decodeIntForKey:@"scoreAvator2"];
        scoreAvator3 = [coder decodeIntForKey:@"scoreAvator3"];
    }
    return self;
}

- (void) encodeWithCoder: (NSCoder *)coder
{
    [coder encodeObject:avator1 forKey:@"avator1"];
    [coder encodeObject:avator2 forKey:@"avator2"];
    [coder encodeObject:avator3 forKey:@"avator3"];
    [coder encodeInt:currentAvator forKey:@"currentAvator"];
    [coder encodeBool:isMusicOn forKey:@"isMusicOn"];
    [coder encodeBool:isSoundEffectOn forKey:@"isSoundEffectOn"];
    [coder encodeInt:scoreAvator1 forKey:@"scoreAvator1"];
    [coder encodeInt:scoreAvator2 forKey:@"scoreAvator2"];
    [coder encodeInt:scoreAvator3 forKey:@"scoreAvator3"];
}

@end
