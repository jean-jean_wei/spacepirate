//
//  UserPreferenceController.m
//  math1
//
//  Created by Jean-Jean Wei on 13-01-19.
//  Copyright (c) 2013 Ice Whale. All rights reserved.
//

#import "UserPreferenceController.h"

@implementation UserPreferenceController

@synthesize userPref;

+ (UserPreferenceController*)instance
{
    static UserPreferenceController* instance = nil;
    
    if (!instance)
    {
        instance = [UserPreferenceController new];
    }
    
    return instance;
}

- (void)startNewSettings
{
    userPref = [UserPreferences new];
    [self saveDataToDisk];
}

- (void)manageSettings
{
    if ([UserPreferenceController.instance shouldStartNewSettings])
    {
        userPref = [UserPreferences new];
        [self saveDataToDisk];
    }
    else
    {
        [self loadDataFromDisk];
    }
}

- (BOOL)shouldStartNewSettings
{
    BOOL startNew = NO;
    
    NSString *hasProgress = [[NSUserDefaults standardUserDefaults] objectForKey:@"hasSettings"];
    if (!hasProgress)
    {
        startNew = YES;
        [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"hasSettings"];
    }
    return startNew;
}

- (NSString*)filePath:(NSString*)filename
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:filename];
    return filePath;
}

- (void)saveDataToDisk
{
    NSString * path = [self filePath:@"userPref"];
    [NSKeyedArchiver archiveRootObject:userPref toFile: path];
}

- (void)loadDataFromDisk
{
    NSString *path = [self filePath:@"userPref"];
    userPref =  [NSKeyedUnarchiver unarchiveObjectWithFile:path];
}

- (int)getLevelClearTime:(int)level
{
    int returnInt;
    
    switch (GameManager.instance.avator)
    {
        case 1:
            returnInt =  [[userPref.avator1 objectAtIndex:level] intValue];
            break;
        case 2:
            returnInt =  [[userPref.avator2 objectAtIndex:level] intValue];
            break;
        case 3:
            returnInt =  [[userPref.avator3 objectAtIndex:level] intValue];
            break;
    }
    
    return returnInt;
}

- (int)saveLevelClearTime:(int)level time:(int)t
{
    int isNewRecord = NO;
    int record = 0;
    switch (userPref.currentAvator)
    {
        case 1:
            record =  [[userPref.avator1 objectAtIndex:level] intValue];
            if (!record || t<record) {
                [userPref.avator1 replaceObjectAtIndex:level withObject:[NSNumber numberWithInt:t]];
                [self saveDataToDisk];
                isNewRecord = t;
            }
            break;
            
        case 2:
            record =  [[userPref.avator2 objectAtIndex:level] intValue];
            if (!record || t<record) {
                [userPref.avator2 replaceObjectAtIndex:level withObject:[NSNumber numberWithInt:t]];
                [self saveDataToDisk];
                isNewRecord = t;
            }
            break;
            
        case 3:
            record =  [[userPref.avator3 objectAtIndex:level] intValue];
            if (!record || t<record) {
                [userPref.avator3 replaceObjectAtIndex:level withObject:[NSNumber numberWithInt:t]];
                [self saveDataToDisk];
                isNewRecord = t;
            }
            break;
    }
    return isNewRecord;
}
- (int)addCurrentScore:(int)a score:(int)s
{
    int returnValue = 0;
    switch (a) {
        case 1:
            returnValue = userPref.scoreAvator1+s;
            userPref.scoreAvator1 = returnValue;
            break;
        case 2:
            returnValue = userPref.scoreAvator2+s;
            userPref.scoreAvator2 = returnValue;
            break;
        case 3:
            returnValue = userPref.scoreAvator3+s;
            userPref.scoreAvator3 = returnValue;
            break;
        default:
            break;
    }
    [self saveDataToDisk];
    
    return returnValue;
}

- (int)getCurrentAvator
{
    return userPref.currentAvator;
}

- (BOOL)getMusic
{
    return userPref.isMusicOn;
}

- (BOOL)getSoundEffect
{
    return userPref.isSoundEffectOn;
}

//- (void)setScore1:(int)num
//{
//    userPref.name = str;
//}
//- (void)setTotal:(int)num
//{
//    userPref.total = num;
//}
//- (void)setD1:(int)num
//{
//    userPref.d1 = num;
//}
//- (void)setD2:(int)num
//{
//    userPref.d2 = num;
//}

- (void)setCurrentAvator:(int)b
{
    userPref.currentAvator = b;
}

- (void)setSoundEffect:(BOOL)b
{
    userPref.isSoundEffectOn = b;
    [self saveDataToDisk];
}

- (void)setMusic:(BOOL)b
{
    userPref.isMusicOn = b;
    [self saveDataToDisk];
}
@end
