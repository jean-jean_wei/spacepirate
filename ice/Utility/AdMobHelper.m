//
//  AdMobHelper.m
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-25.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "AdMobHelper.h"
#import "AppDelegate.h"

#define ADMOB_BANNER_UNIT_ID @"a1513b952860b6e"

@implementation AdMobHelper

+ (AdMobHelper*)instance
{
    static dispatch_once_t pred = 0;
    __strong static AdMobHelper *_instance = nil;
    
    dispatch_once(&pred, ^{
        
        _instance = [[self alloc] init]; //init method
        
    });
    return _instance;
    
}

- (id)init
{                                                        // 8
    self = [super init];
    if (self != nil) {
        // AdMobHelper initialized
        CCLOG(@"AdMobHelper Singleton, init");
        [self initGADBanner];
    }
    return self;
}

- (void)initGADBanner {
    
    // NOTE:
    // Add your publisher ID here and fill in the GADAdSize constant for
    // the ad you would like to request.
    
    
    bannerView_ = [[GADBannerView alloc] initWithAdSize:kGADAdSizeBanner];
    bannerView_.adUnitID = ADMOB_BANNER_UNIT_ID;
    bannerView_.delegate = self;
    
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    [bannerView_ setRootViewController:[app navController]];
    
    //[bannerView_ setRootViewController:self];
    
    //[self.view addSubview:bannerView_];
    //[bannerView_ loadRequest:[self createRequest]];
    // Use the status bar orientation since we haven't signed up for
    // orientation change notifications for this class.
    //[self resizeViews];
}

- (void)setupadMob {
    GADRequest *request = [GADRequest request];
//    request.testing = YES;
//    request.testDevices = [NSArray arrayWithObjects:@"10fe168f8214231ef19b3dce1d6ba498", nil];
    screenSize = [CCDirector sharedDirector].winSize;
    bannerView_.hidden = YES;
    bannerView_ = [[GADBannerView alloc]
                   //      initWithFrame:CGRectMake(0.0, self.view.frame.size.height-GAD_SIZE_320x50.height,
                   initWithFrame:CGRectMake(0.0f, screenSize.height,
                                            GAD_SIZE_320x50.width,
                                            GAD_SIZE_320x50.height)];
  
    
    //bannerView_.frame	= CGRectOffset(bannerView_.frame, x, 320);
    bannerView_.hidden = NO;
    
    //[self createObjs];
    
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    bannerView_.adUnitID = ADMOB_BANNER_UNIT_ID;
    
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    //bannerView_.rootViewController = self;
    
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    [bannerView_ setRootViewController:[app navController]];
    
    
    [[CCDirector sharedDirector].view addSubview:bannerView_];
    //set delegate
    bannerView_.delegate = self;
    // Initiate a generic request to load it with an ad.
    // [bannerView_ loadRequest:[GADRequest request]];
    [bannerView_ loadRequest:request];
    
    adMobVisible = NO;
}

- (void) hideAdMob: (GADBannerView*)banner {
    if (!adMobVisible)
		return;
    
    [UIView beginAnimations:@"BannerOFF" context:nil];
    
    banner.frame = CGRectMake(0, screenSize.height, banner.frame.size.width, banner.frame.size.height);

    
    [UIView commitAnimations];
    
    adMobVisible = NO;
}

- (void) showAdMob: (GADBannerView*)banner {
    if (adMobVisible)
		return;
    [UIView beginAnimations:@"BannerON" context:nil];
    
    
	//banner.frame = CGRectOffset(banner.frame, 0, -banner.frame.size.height);

    banner.frame = CGRectMake(0, screenSize.height-banner.frame.size.height, banner.frame.size.width, banner.frame.size.height);
    [UIView commitAnimations];
    
    adMobVisible = YES;
}

// admob delegate
- (void)adView:(GADBannerView *)bannerView didFailToReceiveAdWithError:(GADRequestError *)error {
    [self hideAdMob:bannerView];
      NSLog(@"AdMob---adView:didFailToReceiveAdWithError:%@", [error localizedDescription]);
    
}

- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    [self showAdMob:bannerView];
      NSLog(@"AdMob---adView:YES AD");
    
}
@end
