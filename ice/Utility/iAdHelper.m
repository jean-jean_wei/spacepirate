//
//  iAdHelper.m
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-25.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "iAdHelper.h"
#import "AppDelegate.h"

@implementation iAdHelper

+ (iAdHelper*)instance
{
    static dispatch_once_t pred = 0;
    __strong static iAdHelper *_instance = nil;
    
    dispatch_once(&pred, ^{
        
        _instance = [[self alloc] init]; //init method
        
    });
    return _instance;
    
}

- (id)init
{                                                        // 8
    self = [super init];
    if (self != nil) {
        // iAdHelper initialized
        CCLOG(@"iAdHelper Singleton, init");
    }
    return self;
}

#pragma mark -
#pragma mark iAd setup methods


- (void)setupiAd
{
    
	
	// Initialize a new iAd banner view
	adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
	
	// Set the delegate and hide the view initially
	adView.delegate		= self;
	adView.hidden		= YES;
	
	// Different iOS versions support different view sizes
	// but we just wanna add a portrait ad view
	if(&ADBannerContentSizeIdentifierPortrait != nil)
		// Size identifier for IPAD + IPHONE
		adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierPortrait;
    //	else
    // Size identifier only for IPHONE
    //		adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifier320x50;
	
	// Move the banner view to the background (across the screen top)
    //int x = UIScreen.mainScreen.bounds.size.height - adView.frame.size.width;
	adView.frame = CGRectOffset(adView.frame, 0, -adView.frame.size.height);
    adView.hidden	= NO;
	
    // restore objects
    //[self createObjs];
	// Add the banner to the current view
	//[self.view addSubview:adView];
//    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
//    [adView setRootViewController:[app navController]];
    
    
    [[CCDirector sharedDirector].view addSubview:adView];
	// Set status of iAd banner
	iAdVisible = NO;
}

- (void) hideBannerView:(ADBannerView*)banner
{
//	if (!iAdVisible)
//		return;
//    
	[UIView beginAnimations:@"animateAdBannerHide" context:NULL];
    banner.frame = CGRectMake(0, -banner.frame.size.height, banner.frame.size.width, banner.frame.size.height);
	[UIView commitAnimations];
    
	// Update visibility state
	iAdVisible = NO;
}

- (void) showBannerView:(ADBannerView*)banner
{
//	if (iAdVisible)
//		return;
	
	[UIView beginAnimations:@"animateAdBannerShow" context:NULL];
	//banner.frame = CGRectOffset(banner.frame, 0, banner.frame.size.height);
    banner.frame = CGRectMake(0, 0, banner.frame.size.width, banner.frame.size.height);
    [UIView commitAnimations];
    
    // Update visibility state
	iAdVisible = YES;
}

#pragma mark -
#pragma mark iAd delegate methods


// This method is invoked each time a banner loads a new advertisement. Once a banner has loaded an ad,
// it will display that ad until another ad is available. The delegate might implement this method if
// it wished to defer placing the banner in a view hierarchy until the banner has content to display.
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
	[self showBannerView:banner];
    NSLog(@"YES, iAD");
}

// This method will be invoked when an error has occurred attempting to get advertisement content.
// The ADError enum lists the possible error codes.
- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
	// In any error case hide the banner
	[self hideBannerView:banner];
	
    NSLog(@"iAD BannerView didFailToReceiveAdWithError: %d, %@, %@",
          [error code],
          [error domain],
          [error localizedDescription]);
}

// This message will be sent when the user taps on the banner and some action is to be taken.
// Actions either display full screen content in a modal session or take the user to a different
// application. The delegate may return NO to block the action from taking place, but this
// should be avoided if possible because most advertisements pay significantly more when
// the action takes place and, over the longer term, repeatedly blocking actions will
// decrease the ad inventory available to the application. Applications may wish to pause video,
// audio, or other animated content while the advertisement's action executes.
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
	return YES;
}

// This message is sent when a modal action has completed and control is returned to the application.
// Games, media playback, and other activities that were paused in response to the beginning
// of the action should resume at this point.
- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
}


@end
