//
//  AdMobHelper.h
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-25.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GADBannerView.h"

@interface AdMobHelper : NSObject <GADBannerViewDelegate>
{
    GADBannerView *bannerView_;
    BOOL adMobVisible;
    CGSize screenSize;
}

+ (AdMobHelper*)instance;
- (void)setupadMob;

@end
