//
//  AvatorListLayer.h
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-26.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "CCLayer.h"

@interface AvatorListLayer : CCLayer
{
    CCMenu *listMenu;
    CCMenuItemSprite *menuItem1;
    CCMenuItemSprite *menuItem2;
    CCMenuItemSprite *menuItem3;
    CCLabelTTF *lblGo;
}
@end
