//
//  GameBlock.m
//  Ice
//
//  Created by Jean-Jean Wei on 2013-10-31.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "GameBlock.h"

@implementation GameBlock

@synthesize characterState;

-(void)changeState:(CharacterStates)newState
{
    [self stopAllActions];
    id action = nil;
    //  self.characterState = newState;
    
    switch (newState)
    {
        case kStateIdle:
            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kBreathingAnim]];
            break;
            
            //        case kStateWalking:
            //            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kWalkingAnim]];
            //            break;
            //
            //        case kStateCrouching:
            //            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kCrouchingAnim]];
            //            break;
            //
            //
            //        case kStateBreathing:
            //            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kBreathingAnim]];
            //            break;
            //
            //        case kStatePreIdle:
            //            action = [CCAnimate actionWithAnimation:[ccAnimArray objectAtIndex:kPreIdlingAnim]];
            //            self.characterState = kStateIdle;
            //            break;
            
        default:
            break;
    }
    if (action != nil)
    {
        [self runAction:action];
    }
}


#pragma mark -
-(void)updateStateWithDeltaTime:(ccTime)deltaTime andListOfGameObjects:(CCArray*)listOfGameObjects
{
    
    if ([self numberOfRunningActions] == 0)
    {
        [self changeState:kStateIdle];
//        if (self.characterState == kStatePreIdle)
//        {
//            [self changeState:kStateIdle];
//        }
//        else
//        {
//            [self changeState:self.characterState];
//        }
        
        
    }
    
}


@end
