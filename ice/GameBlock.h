//
//  GameBlock.h
//  Ice
//
//  Created by Jean-Jean Wei on 2013-10-31.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "GameObject.h"

@interface GameBlock : GameObject
{
    NSMutableArray *ccAnimArray;
    CharacterStates characterState;
    ccTime delta;
    BOOL is2x2;
    
    // new variable for untouchable feature
    BOOL notTouchable;
    CGPoint oldPosition;
}

@property (assign) CharacterStates characterState;
@end
