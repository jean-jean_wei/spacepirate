//
//  TileN1x1.h
//  Ice
//
//  Created by Jean-Jean Wei on 2013-10-27.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "GameCharacter.h"

@interface Block1x1 : GameCharacter
{
    NSMutableArray *ccAnimArray;
    CharacterAnimTypes *ccAnimTypes;
    float millisecondsStayingIdle;
    //    GameCharacter *accessary;
}

- (id)initWithPosition:(int)pos;// andSprite:(GameItem*)obj;

@end
