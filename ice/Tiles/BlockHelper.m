//
//  BlockHelper.m
//  ice
//
//  Created by Jean-Jean Wei on 1/14/2014.
//  Copyright (c) 2014 Jean-Jean Wei. All rights reserved.
//

#import "BlockHelper.h"

@implementation BlockHelper

+ (BlockHelper*)instance
{
    static dispatch_once_t pred = 0;
    __strong static BlockHelper *_instance = nil;
    
    //static SoundManager *instance = nil;
    dispatch_once(&pred, ^{
        
        _instance = [[self alloc] init]; //init method
        
    });
    return _instance;
    
}

- (id)init
{                                                        // 8
    self = [super init];
    if (self != nil)
    {
        // BlockHelper initialized
        CCLOG(@"BlockHelper Singleton, init");
        [self loadBlockPlist];
        [self createBlockPosition];
    }
    return self;
}


- (void)loadBlockPlist
{
    NSString *fullFileName = @"Blocks.plist";
    NSString *plistPath;
    
    // 1: Get the Path to the plist file
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                              NSUserDomainMask, YES)
                          objectAtIndex:0];
    plistPath = [rootPath stringByAppendingPathComponent:fullFileName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"Blocks" ofType:@"plist"];
    }
    
    // 2: Read in the plist file
    blockDeployment = [NSArray arrayWithContentsOfFile:plistPath];
    
    // 3: If the plistDictionary was null, the file was not found.
    if (!blockDeployment || blockDeployment.count == 0) {
        CCLOG(@"Error reading Levels.plist");
        return; // No Plist Dictionary or file found
    }
}

- (void)createBlockPosition
{
    NSMutableArray *temp = [NSMutableArray new];
    
    float startX = RIGHTBOUND + 19.0f;
    float offX = 38.0f;
    
    float startY = UPBOUND + 19.0f;
    float offY = 38.0f;
    
    for (int i=0; i<9; i++)
    {
        float numY = startY - offY * i;
        for (int j=0; j<9; j++)
        {
            float numX = startX - offX*j;
            [temp addObject:[NSValue valueWithCGPoint:CGPointMake(numX, numY)]];
        }
        
    }
    blockPosition = temp;
}

- (NSArray*)getMatrix:(int)idx
{
    return [blockDeployment objectAtIndex:idx];
}

- (CGPoint)getPosition:(int)idx
{
    CGPoint p = [[blockPosition objectAtIndex:idx] CGPointValue];
    return p;
}

- (void)print
{
    NSLog(@"blockDeployment= %@",blockDeployment);
    for (NSArray *item in blockDeployment) {
        NSLog(@"block= %@",item);
        for (NSString *deploy in item) {
            
            NSArray *element = [deploy componentsSeparatedByString:@","];
            for (int i = 0; i< element.count; i++) {
                NSLog(@"i = %i, VALUE = %i",i, [[element objectAtIndex:i] intValue]);
            }
        }
    }
    
}

@end
