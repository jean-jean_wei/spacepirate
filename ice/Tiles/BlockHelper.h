//
//  BlockHelper.h
//  ice
//
//  Created by Jean-Jean Wei on 1/14/2014.
//  Copyright (c) 2014 Jean-Jean Wei. All rights reserved.
//

#import "GameObject.h"

@interface BlockHelper : NSObject
{
    NSArray *blockDeployment;
    NSArray *blockPosition;
}

+ (BlockHelper*)instance;
- (NSArray*)getMatrix:(int)idx;
- (CGPoint)getPosition:(int)idx;

- (void)print;
@end
