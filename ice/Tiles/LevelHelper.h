//
//  LevelHelper.h
//  puzzle
//
//  Created by Jean-Jean Wei on 13-02-24.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LevelHelper : NSObject
{
    NSArray *levelDeployment;
}

+ (LevelHelper*)instance;
- (NSArray*)getLevelMatrix:(int)idx;

- (void)print;
@end
