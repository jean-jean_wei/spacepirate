//
//  BarHx7.m
//  Ice
//
//  Created by Jean-Jean Wei on 2013-10-30.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "Boarder.h"
#import "BlockHelper.h"
@implementation Boarder


#pragma mark -
-(void)initAnimations:(int)type
{
    // load animations from Classname.plist to animation array
    NSString *str = NSStringFromClass([self class]);
    ccAnimArray = [NSMutableArray new];
//    for (int i = 0; i<kNumOfCharacterAnimeTypes; i++)
//    {
        CCAnimation *temp = [self loadPlistForAnimationWithName:[ANIM_NAMES objectAtIndex:type] andClassName:str];
        [ccAnimArray addObject:temp];
 //   }
    
}


#pragma mark -

- (id)initWithPosition:(int)pos andType:(BoarderType)type
{
    
    self = [super init];
    if (self != nil)
    {
        notTouchable = YES;
        is2x2 = NO;
        CGPoint p = [BlockHelper.instance getPosition:pos];
        
       [self initAnimations:type];
        
        switch (type)
        {
            case kBoarder_v:
                [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"boarderV_1.png"]];
                break;
            case kBoarder_h:
                [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"boarderH_1.png"]];
                break;
            case kBoarder_upLeft:
                [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"boarderUpLeft_1.png"]];
                break;
            case kBoarder_upRight:
                [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"boarderUpRight_1.png"]];
                break;
            case kBoarder_downLeft:
                [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"boarderDownLeft_1.png"]];
                break;
            case kBoarder_downRight:
                [self setDisplayFrame:[[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:@"boarderDownRight_1.png"]];
                break;
                
            default:
                break;
        }
        
        
        
                
                
                   
        self.position = p;
        oldPosition = p;
        
        
        millisecondsStayingIdle = 0.0f;
        
       
        
    }
    return self;
}


@end
