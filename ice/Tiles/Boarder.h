//
//  BarHx7.h
//  Ice
//
//  Created by Jean-Jean Wei on 2013-10-30.
//  Copyright (c) 2013 Ice Whale Inc. All rights reserved.
//

#import "GameBlock.h"

@interface Boarder : GameBlock
{
   
    CharacterAnimTypes *ccAnimTypes;
    float millisecondsStayingIdle;
    //    GameCharacter *accessary;
}

- (id)initWithPosition:(int)pos andType:(BoarderType)type;


@end
