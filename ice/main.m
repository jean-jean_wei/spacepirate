//
//  main.m
//  ice
//
//  Created by Jean-Jean Wei on 12/27/2013.
//  Copyright Jean-Jean Wei 2013. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
        return retVal;
    }
}
